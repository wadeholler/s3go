package main

import (
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

func exitErrorf(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, msg+"\n", args...)
	os.Exit(1)
}

func main() {
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	// Create S3 service client
	svc := s3.New(sess)

	if len(os.Args) != 2 {
		exitErrorf("bucket name required\nUsage: %s bucket_name", os.Args[0])
	}
	bucket := os.Args[1]

	params := &s3.CreateBucketInput{
		Bucket: aws.String(bucket), // Required
		/*
			    ACL:    aws.String("BucketCannedACL"),
					CreateBucketConfiguration: &s3.CreateBucketConfiguration{
						LocationConstraint: aws.String("BucketLocationConstraint"),
					},
		*/
		/*
			    GrantFullControl: aws.String("GrantFullControl"),
					GrantRead:        aws.String("GrantRead"),
					GrantReadACP:     aws.String("GrantReadACP"),
					GrantWrite:       aws.String("GrantWrite"),
					GrantWriteACP:    aws.String("GrantWriteACP"),
		*/
	}
	resp, err := svc.CreateBucket(params)

	if err != nil {
		// Print the error, cast err to awserr.Error to get the Code and
		// Message from an error.
		fmt.Println(err.Error())
		return
	}

	// Pretty-print the response data.
	fmt.Println(resp)

}
