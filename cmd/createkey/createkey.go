package main

import (
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kms"
)

func exitErrorf(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, msg+"\n", args...)
	os.Exit(1)
}

func main() {
	if len(os.Args) != 2 {
		exitErrorf("profile name required\nUsage: %s profile", os.Args[0])
	}
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
		Profile:           os.Args[1],
	}))

	svc := kms.New(sess)

	params := &kms.CreateKeyInput{
		BypassPolicyLockoutSafetyCheck: aws.Bool(true),
		Description:                    aws.String("KMS KEY FOR S3 TESTING"),
		//KeyUsage:                       aws.String("KeyUsageType"),
		//Origin:                         aws.String("OriginType"),
		//Policy:                         aws.String("PolicyType"),
		Tags: []*kms.Tag{
			{ // Required
				TagKey:   aws.String("kmsS3loc"),   // Required
				TagValue: aws.String("commercial"), // Required
			},
			// More values...
		},
	}
	resp, err := svc.CreateKey(params)

	if err != nil {
		// Print the error, cast err to awserr.Error to get the Code and
		// Message from an error.
		fmt.Println(err.Error())
		return
	}

	// Pretty-print the response data.
	fmt.Println(resp)
}
