package main

import (
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
)

func exitErrorf(msg string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, msg+"\n", args...)
	os.Exit(1)
}

func main() {

	if len(os.Args) != 3 {
		exitErrorf("bucket name and object name/key are required\nUsage: %s bucket_name", os.Args[0])
	}
	bucket := os.Args[1]
	object := os.Args[2]

	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	// Create S3 service client
	svc := s3.New(sess)
	params := &s3.HeadObjectInput{
		Bucket: aws.String(bucket), // Required
		Key:    aws.String(object), // Required
		/*
				 IfMatch:              aws.String("IfMatch"),
			   IfModifiedSince:      aws.Time(time.Now()),
			   IfNoneMatch:          aws.String("IfNoneMatch"),
			   IfUnmodifiedSince:    aws.Time(time.Now()),
			   PartNumber:           aws.Int64(1),
			   Range:                aws.String("Range"),
			   RequestPayer:         aws.String("RequestPayer"),
			   SSECustomerAlgorithm: aws.String("SSECustomerAlgorithm"),
			   SSECustomerKey:       aws.String("SSECustomerKey"),
			   SSECustomerKeyMD5:    aws.String("SSECustomerKeyMD5"),
			   VersionId:            aws.String("ObjectVersionId"),
		*/
	}
	resp, err := svc.HeadObject(params)

	if err != nil {
		// Print the error, cast err to awserr.Error to get the Code and
		// Message from an error.
		fmt.Println(err.Error())
		return
	}

	// Pretty-print the response data.
	fmt.Println(resp)
}
